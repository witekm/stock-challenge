require 'rails_helper'
require 'support/contexts/stock_exchange_context'
require_relative '../../../../app/ddd/command_bus'

RSpec.describe ::Context::StockExchange::Domain::StockOwnership, type: :request do
  include CommandBus
  include_context "stock exchange context"

  let(:params) do
    {
        bearer_id: bob.id,
        market_price_id: market_price_PLN.id,
        name: 'ORLEN'
    }
  end

  describe 'Create Stock - Command' do
    subject { execute(::Context::StockExchange::Commands::CreateStock.new(current_user_id:  1,
                                                                  stock_name:      params[:name],
                                                                  bearer_id:       params[:bearer_id],
                                                                  market_price_id: params[:market_price_id]
            )) }

    context 'with wrong data' do
      context 'when params are missing' do

        context 'with lack of bearer_id' do
          it 'raises error RecordGeneralError' do
            params[:bearer_id] = nil

            expect{ subject }.to raise_error(RecordGeneralError, /bearer_id/)
          end

          it 'raises error with message about bearer_id' do
            params[:bearer_id] = nil

            expect{ subject }.to raise_error(RecordGeneralError,/can't be blank/)
          end
        end

        context 'with lack of market_price_id' do
          it 'raises error RecordGeneralError' do
            params[:market_price_id] = nil

            expect{ subject }.to raise_error(RecordGeneralError, /market_price_id/)
          end

          it 'raises error with message about market_price_id' do
            params[:market_price_id] = nil

            expect{ subject }.to raise_error(RecordGeneralError,/can't be blank/)
          end
        end
      end

      context 'when stock name is not uniq' do
        it 'raises error ' do
          Stock.create(name: params[:name], bearer_id: 1, market_price_id: 1)

          expect{ subject }.to raise_error(RecordGeneralError,/Name has already been taken/)
        end
      end
    end
  end

  let(:update_params) do
    {
        id: kghm_stock.id,
        stock: {
            name: 'XZY',
            market_price: { currency: 'PLN', value_cents: 777 },
            bearer: { name: 'Tim Nowak' }
        }
    }
  end

  describe 'Update Stock - Command' do
    subject { execute(::Context::StockExchange::Commands::UpdateStock.new(current_user_id:  1,
                                                                          stock_name:        update_params[:stock][:name],
                                                                          stock_id:          update_params[:id],
                                                                          bearer_name:       update_params[:stock][:bearer][:name],
                                                                          market_price_currency:    update_params[:stock][:market_price][:currency],
                                                                          market_price_value_cents: update_params[:stock][:market_price][:value_cents]
    )) }

    context 'with wrong data' do
      context 'when params are missing' do

        context 'with lack of stock name' do
          it 'raises error RecordGeneralError' do
            update_params[:stock][:name] = nil

            expect{ subject }.to raise_error(RecordGeneralError, /stock_name/)
          end
        end

        context 'with lack of bearer name' do
          it 'raises error RecordGeneralError' do
            update_params[:stock][:bearer][:name] = nil

            expect{ subject }.to raise_error(RecordGeneralError, /bearer_name/)
          end
        end

        context 'with lack of market_price currency' do
          it 'raises error RecordGeneralError' do
            update_params[:stock][:market_price][:currency] = nil

            expect{ subject }.to raise_error(RecordGeneralError, /market_price_currency/)
          end
        end

        context 'with lack of market_price value_cents' do
          it 'raises error RecordGeneralError' do
            update_params[:stock][:market_price][:value_cents] = nil

            expect{ subject }.to raise_error(RecordGeneralError, /market_price_value_cents/)
          end
        end
      end

      context 'when record can not be found' do
        it 'raises error RecordNotFoundError' do
          update_params[:id] = 100

          expect{ subject }.to raise_error(RecordNotFoundError, /Couldn't find Stock with 'id'=100/)
        end
      end
    end
  end

  let(:delete_params) do
    {
      id: kghm_stock.id
    }
  end

  describe 'SoftDeleteStock - Command' do
    subject { execute(::Context::StockExchange::Commands::SoftDeleteStock.new(current_user_id: 1, stock_id: delete_params[:id])) }

    context 'with wrong data' do
      context 'when params are missing' do

        context 'with lack of stock_id' do
          it 'raises error RecordGeneralError' do
            delete_params[:id] = nil

            expect{ subject }.to raise_error(RecordGeneralError, /stock_id/)
          end
        end
      end
    end
  end
end
