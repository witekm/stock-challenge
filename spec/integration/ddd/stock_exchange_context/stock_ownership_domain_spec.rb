require 'rails_helper'
require 'support/contexts/stock_exchange_context'

RSpec.describe ::Context::StockExchange::Domain::StockOwnership, type: :request do
  include_context "stock exchange context"

  let(:params) do
    {
        bearer_id: bob.id,
        market_price_id: market_price_PLN.id,
        name: 'ORLEN'
    }
  end

  describe 'POST /api/stocks' do
    subject { post '/api/stocks', params: { stock: params }, as: :json }

    context 'with proper data' do

      it 'creates new stock record' do
        expect { subject }.to change(Stock, :count).by(1)
      end

      it 'saves create event in stock ownership stream' do
        subject

        stream_name = "StockOwnership$#{Stock.find_by_name(params[:name]).id}"
        event_store = Rails.configuration.event_store

        expect(event_store).to have_published(
                                   an_event(::Context::StockExchange::Domain::StockCreated)
                                   .with_data(stock_name: params[:name],
                                              stock_bearer_id: params[:bearer_id],
                                              stock_market_price_id: params[:market_price_id])
                               ).in_stream(stream_name)
      end
    end
  end

  let(:put_params) do
    {
        id: kghm_stock.id,
        stock: {
            name: 'XZY',
            market_price: { currency: 'PLN', value_cents: 777 },
            bearer: { name: 'Tim Nowak' }
        }
    }
  end

  describe 'PUT /api/stocks/:id' do
    subject { put "/api/stocks/#{put_params[:id]}", params: put_params, as: :json }

    context 'with proper data' do

      it 'updates stock record' do
        subject
        expect(Stock.find_by_name(put_params[:stock][:name])).to be_present
      end

      it 'saves update event in stock ownership stream' do
        subject

        stream_name = "StockOwnership$#{kghm_stock.id}"
        event_store = Rails.configuration.event_store

        expect(event_store).to have_published(
               an_event(::Context::StockExchange::Domain::StockUpdated)
                   .with_data(stock_name: put_params[:stock][:name],
                              stock_bearer_id: Bearer.find_by_name(put_params[:stock][:bearer][:name]).id,
                              stock_market_price_id: MarketPrice.where(currency: put_params[:stock][:market_price][:currency],
                                                                       value_cents: put_params[:stock][:market_price][:value_cents]).first.id))
                   .in_stream(stream_name)
      end
    end
  end

  let(:delete_params) do
    {
        id: kghm_stock.id
    }
  end

  describe 'PUT /api/stocks/:id' do
    subject { delete "/api/stocks/#{put_params[:id]}", params: delete_params, as: :json }

    context 'with proper data' do

      it 'deletes stock record' do
        subject
        expect(Stock.find_by_name(kghm_stock.name)).not_to be_present
      end

      it 'saves delete event in stock ownership stream' do
        subject

        stream_name = "StockOwnership$#{kghm_stock.id}"
        event_store = Rails.configuration.event_store

        expect(event_store).to have_published(
                                   an_event(::Context::StockExchange::Domain::StockSoftDeleted)
                                 ).in_stream(stream_name)
      end
    end
  end
end