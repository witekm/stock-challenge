require "rails_helper"
require 'support/contexts/stock_exchange_context'

RSpec.describe Api::StocksController, :type => :controller do
  include_context "stock exchange context"

  describe "GET index" do
    subject { get :index, as: :json }

    it "render @stocks successfully" do
      subject

      expect(response.status).to eq(200)
    end

    it 'renders stocks with info from bearer and market_price' do
      subject

      expect(json_response.first[:stock_name]).to eq kghm_stock.name
      expect(json_response.first[:bearer_name]).to eq kghm_stock.bearer.name
      expect(json_response.first[:market_price_currency]).to eq kghm_stock.market_price.currency
      expect(json_response.first[:market_price_value_cents]).to eq kghm_stock.market_price.value_cents
    end

    it 'renders all records' do
      subject

      expect(json_response.size).to eq 2
    end
  end

  describe "POST create" do
    let(:params) do
      {
          bearer_id: bob.id,
          market_price_id: market_price_PLN.id,
          name: 'ORLEN'
      }
    end

    subject { post :create, params: { stock: params  }, as: :json }

    context 'with proper data' do
      it "creates new stock with bearer and market_price referenced by id" do

        expect { subject }.to change(Stock, :count).by(1)

        expect(response).to have_http_status(:success)
      end

      it 'creates record with given data' do
        subject
        expect(Stock.find_by(bearer_id: bob.id, market_price_id: market_price_PLN.id).name).to eq params[:name]
      end
    end

    context 'with wrong data' do
      context 'when params are missing' do
        it 'returns 422 - unprocessable entity for bearer_id nil' do
          params[:bearer_id] = nil

          subject

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns 422 - unprocessable entity for market_price_id nil' do
          params[:market_price_id] = nil

          subject

          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context 'when stock name is not uniq' do
        it 'returns 422 - unprocessable entity' do
          Stock.create(name: params[:name], bearer_id: 1, market_price_id: 1)

          subject

          expect(response).to have_http_status(:unprocessable_entity)
          expect(json_response['message']).to match /Name has already been taken/
        end
      end
    end
  end

  describe "PUT update" do
    let(:params) do
      {
          id: kghm_stock.id,
          stock: {
              name: 'XZY',
              market_price: { currency: 'PLN', value_cents: 777 },
              bearer: { name: 'Tim Nowak' }
          }
      }
    end

    subject { put :update, params: params, as: :json }

    context 'with proper data' do
      context 'with new related data' do
        it 'updates stock with new name' do
          subject
          expect(Stock.find_by_name(params[:stock][:name])).to be_present
        end

        it 'updates record and creates new market_price' do
          expect{ subject }.to change(MarketPrice, :count).by(1)
          updated_stock = Stock.find_by_name(params[:stock][:name])

          expect(updated_stock.market_price.value_cents).to eq params[:stock][:market_price][:value_cents]
        end

        it 'updates record and creates new bearer' do
          expect{ subject }.to change(Bearer, :count).by(1)
          updated_stock = Stock.find_by_name(params[:stock][:name])

          expect(updated_stock.bearer.name).to eq params[:stock][:bearer][:name]
        end
      end

      context 'with partialy existing data' do
        context 'with old bearer and new market price' do
          it 'works fine' do
            params[:stock][:bearer][:id] = john.id
            params[:stock][:bearer][:name] = john.name

            expect{ subject }.to change(MarketPrice, :count).by(1)

            updated_stock = Stock.find_by_name(params[:stock][:name])
            expect(updated_stock.bearer.id).to eq john.id
          end
        end

        context 'with old market price and new bearer' do
          it 'works fine' do
            params[:stock][:market_price][:id] = market_price_EUR.id
            params[:stock][:market_price][:currency] = market_price_EUR.currency
            params[:stock][:market_price][:value_cents] = market_price_EUR.value_cents

            expect{ subject }.to change(Bearer, :count).by(1)

            updated_stock = Stock.find_by_name(params[:stock][:name])
            expect(updated_stock.market_price.id).to eq market_price_EUR.id
          end
        end
      end
    end

    context 'with wrong data' do
      context 'when params are missing' do
        it 'returns 422 - unprocessable entity for bearer name nil' do
          params[:stock][:bearer][:name] = nil

          subject

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns 422 - unprocessable entity for market_price currency is nil' do
          params[:stock][:market_price][:currency] = nil

          subject

          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context 'when stock is not found' do
        it 'returns 404 - record not found' do
          params[:id] = 100

          subject

          expect(response).to have_http_status(:not_found)
        end
      end
    end
  end

  describe "DELETE destroy" do
    let(:params) do
      {
          id: kghm_stock.id
      }
    end

    subject { delete :destroy, params: params, as: :json }

    context 'with proper data' do
      it 'removes stock' do
        subject
        expect(Stock.find_by_name(kghm_stock.name)).not_to be_present
      end
    end

    context 'with wrong data' do
      context 'when stock is not found' do
        it 'returns 404 - record not found' do
          params[:id] = 100

          subject

          expect(response).to have_http_status(:not_found)
        end
      end
    end
  end
end