module ResponseHelper

  # @return [ActiveSupport::HashWithIndifferentAccess]
  def json_response
    fix_json_response JSON.parse(response.body)
  rescue
    nil
  end

  def fix_json_response(data)
    if data.is_a?(Array)
      data.map { |d| fix_json_response(d) }
    elsif data.is_a?(Hash)
      data.with_indifferent_access
    else
      data
    end
  end
end

RSpec.configure do |config|
  config.include ResponseHelper
end
