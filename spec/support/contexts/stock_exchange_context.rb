RSpec.shared_context "stock exchange context", :shared_context => :metadata do
  let!(:bob)  { create(:bearer_bob) }
  let!(:alice){ create(:bearer_alice) }
  let!(:john) { create(:bearer, name: 'John') }
  let!(:eva)  { create(:bearer, name: 'Eva') }

  let!(:market_price_PLN) { create(:market_price, currency: 'PLN', value_cents: 1111) }
  let!(:market_price_EUR) { create(:market_price, currency: 'EUR', value_cents: 2222) }

  let!(:kghm_stock)   { create(:stock, name: 'kghm', bearer_id: alice.id, market_price_id: market_price_PLN.id) } # market_price_PLN alice
  let!(:google_stock) { create(:stock, name: 'google', bearer_id: bob.id, market_price_id: market_price_EUR.id) } # market_price_PLN alice

end