FactoryBot.define do
  factory :bearer do
    factory :bearer_bob do
      name "Bob"
    end

    factory :bearer_alice do
      name "Alice"
    end
  end
end
