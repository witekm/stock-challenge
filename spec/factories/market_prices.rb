FactoryBot.define do
  factory :market_price do
    factory :market_price_PLN do
      currency "PLN"
      value_cents 200
    end

    factory :market_price_EUR do
      currency "EUR"
      value_cents 300
    end
  end
end
