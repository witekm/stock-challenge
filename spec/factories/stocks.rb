FactoryBot.define do
  factory :stock do
    association :bearer_id, factory: :bearer_alice
    association :market_price_id, factory: :market_price_PLN
    name "BasicStock"
  end
end
