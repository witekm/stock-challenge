require 'rails_helper'

RSpec.describe MarketPrice, type: :model do

  context 'when initializing' do
    subject(:market_price) { MarketPrice.create!(currency: "PLN", value_cents: 1_000) }

    it 'creates proper object' do
      expect(market_price.currency).to eq 'PLN'
      expect(market_price.value_cents).to eq 1000
    end
  end

  context 'when does not meet criteria' do
    let!(:pricePLN_200) { create(:market_price_PLN) }

    context 'with not uniq currency and value_cents' do
      it 'does not save object' do
        expect(MarketPrice.create(currency: pricePLN_200.currency,
                                  value_cents: pricePLN_200.value_cents).persisted?)
              .to eq false
      end

      it 'raises error' do
        expect{MarketPrice.create!(currency: pricePLN_200.currency,
                                  value_cents: pricePLN_200.value_cents)}
              .to raise_error(ActiveRecord::RecordInvalid, /Value cents is not uniq for currency/)
      end
    end
  end

end
