require 'rails_helper'

RSpec.describe Bearer, type: :model do

  context 'when initializing' do
    subject(:bearer) { Bearer.create(name: 'Bob') }

    it 'creates proper object' do
      expect(bearer.name).to eq 'Bob'
    end
  end

  context 'when does not meet criteria' do
    let!(:bob) { create(:bearer_bob) }

    context 'with not uniq name' do
      it 'does not save object' do
        expect(Bearer.create(name: bob.name).persisted?).to eq false
      end

      it 'raises error' do
        expect{Bearer.create!(name: bob.name)}.to raise_error(ActiveRecord::RecordInvalid)
      end
    end
  end

  context 'with many stocks' do
    let!(:bob) { create(:bearer_bob) }
    subject { bob.stocks.create(name: 'test_stock', market_price: create(:market_price_PLN))
              bob.stocks.create(name: 'test_stock2', market_price: create(:market_price_EUR))}

    it { expect{ subject }.to change { bob.stocks.size }.by(2) }
  end
end
