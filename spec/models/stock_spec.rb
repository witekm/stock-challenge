require 'rails_helper'

RSpec.describe Stock, type: :model do
  context 'when initializing' do
    let(:bearer_bob) { create(:bearer_bob) }
    let(:market_price_EUR) { create(:market_price_EUR)}
    subject(:stock) { Stock.create(name:            'Apple',
                                   bearer_id:       bearer_bob.id,
                                   market_price_id: market_price_EUR.id) }

    it 'creates proper object' do
      expect(stock.name).to eq 'Apple'
      expect(stock.bearer).to eq bearer_bob
      expect(stock.market_price).to eq market_price_EUR
    end
  end
end
