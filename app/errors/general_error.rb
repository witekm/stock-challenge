class GeneralError < BaseError
  def http_status; :internal_server_error; end
end