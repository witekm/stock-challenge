class RecordNotFoundError < RecordGeneralError
  def http_status; :not_found; end
end