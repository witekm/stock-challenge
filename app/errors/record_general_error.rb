class RecordGeneralError < GeneralError
  def http_status; :unprocessable_entity; end
end