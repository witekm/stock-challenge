class BaseError < StandardError
  def http_status
    500
  end
end
