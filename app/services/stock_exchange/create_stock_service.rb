module StockExchange
  class CreateStockService
    class << self
      def call(params)
        self.new.call(params)
      end
    end

    def initialize; end

    def call(params)
      Stock.create!(name:            params[:name],
                    bearer_id:       params[:bearer_id],
                    market_price_id: params[:market_price_id])
    rescue ActiveRecord::RecordInvalid => e
      raise ::RecordInvalidError.new(e.message)
    end
  end
end