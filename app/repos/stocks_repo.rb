class StocksRepo
  def initialize

  end

  def all
    data = Stock.includes(:bearer, :market_price).all.pluck('stocks.name',
                                                     'bearers.name',
                                                     'market_prices.currency',
                                                     'market_prices.value_cents')
    build_hash(data)
  end

  private
  def build_hash(data)
    data.map do |record|
      {
          stock_name: record[0],
          bearer_name: record[1],
          market_price_currency: record[2],
          market_price_value_cents: record[3]
      }
    end
  end
end