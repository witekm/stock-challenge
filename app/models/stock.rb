class Stock < ApplicationRecord
  include SoftDeletable

  belongs_to :bearer
  belongs_to :market_price

  accepts_nested_attributes_for :bearer, :market_price

  validates_uniqueness_of :name
end
