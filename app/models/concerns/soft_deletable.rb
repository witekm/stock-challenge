module SoftDeletable
  extend ActiveSupport::Concern

  included do
    scope :not_deleted,  -> { where(deleted_at: nil) }
    scope :deleted,  -> { where.not(deleted_at: nil) }

    default_scope { not_deleted }

    alias_method :hard_destroy, :destroy

    def soft_destroy
      update(deleted_at: ::DateTime.now)
    end

    alias_method :destroy, :soft_destroy
  end
end