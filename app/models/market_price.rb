class MarketPrice < ApplicationRecord
  include ActiveModel::Validations

  has_many :stocks

  validates_with CurrencyAndValueCentsUniquenessValidator

  monetize :value_cents
end
