class CurrencyAndValueCentsUniquenessValidator < ActiveModel::Validator
  def validate(record)
    if MarketPrice.where(currency: record.currency, value_cents: record.value_cents).exists?
      record.errors[:value_cents] << 'is not uniq for currency'
    end
  end
end