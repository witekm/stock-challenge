module Api
  class StocksController < ApplicationController
    include CommandBus

    def index
      render json: StocksRepo.new.all
    end

    def create
      create_command = ::Context::StockExchange::Commands::CreateStock.new(current_user_id:  current_user,
                                                                           stock_name:      create_stock_params[:name],
                                                                           bearer_id:       create_stock_params[:bearer_id],
                                                                           market_price_id: create_stock_params[:market_price_id])
      execute(create_command)
      head :ok
    end

    def update
      update_command =
          ::Context::StockExchange::Commands::UpdateStock.new(current_user_id: current_user,
                                                              stock_name:      update_stock_params[:name],
                                                              stock_id:        params[:id],
                                                              bearer_name:     update_stock_params[:bearer][:name],
                                                              market_price_currency: update_stock_params[:market_price][:currency],
                                                              market_price_value_cents: update_stock_params[:market_price][:value_cents])
      execute(update_command)
      head :ok
    end

    def destroy
      delete_command = ::Context::StockExchange::Commands::SoftDeleteStock.new(current_user_id: current_user, stock_id: params[:id])
      execute(delete_command)
      head :ok
    end

    private

    def create_stock_params
      params.require(:stock).permit(:bearer_id, :market_price_id, :name)
    end

    def update_stock_params
      params.require(:stock).permit(:name, bearer: [ :id, :name ], market_price: [ :id, :currency, :value_cents])
    end

  end
end

