class ApplicationController < ActionController::Base
  before_action :ensure_json

  rescue_from RecordGeneralError, with: :render_error

  def current_user
   @current_user ||= (rand()*100).round
  end

  private

  def ensure_json
    render json: { message: "Wrong content-type header" }, status: 406 and return unless request.content_type == "application/json"
  end

  def render_error(error)
    render json: { message: error.message }, status: error.http_status
  end

end
