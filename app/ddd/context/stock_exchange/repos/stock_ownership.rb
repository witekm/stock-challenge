require_relative '../../../repository'

module Context
  module StockExchange
    module Repos
      class StockOwnership
        include Repository

        def initialize(orm_adapter: ::Stock)
          super
        end

        def find(stock_id)
          map_record(orm_adapter.find(stock_id))
        end

        private
        def map_record(record)
          Entities::StockOwnership.new.tap do |stock_ownership|
            stock_ownership.id                 = record.id
            stock_ownership.stock_name         = record.name
            stock_ownership.stock_bearer       = Repos::Bearer.new.find(record.bearer_id)
            stock_ownership.stock_market_price = Repos::MarketPrice.new.find(record.market_price_id)
          end
        end
      end
    end
  end
end
