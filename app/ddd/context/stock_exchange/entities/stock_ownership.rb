module Context
  module StockExchange
    module Entities
      class StockOwnership
        include Virtus.model

        attribute :id,                 Integer
        attribute :stock_name,         String
        attribute :stock_bearer,       Bearer
        attribute :stock_market_price, MarketPrice
      end
    end
  end
end