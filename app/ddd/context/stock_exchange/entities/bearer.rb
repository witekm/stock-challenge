module Context
  module StockExchange
    module Entities
      class Bearer
        include Virtus.model

        attribute :id,   Integer
        attribute :name, String
      end
    end
  end
end