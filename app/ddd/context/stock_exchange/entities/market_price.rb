module Context
  module StockExchange
    module Entities
      class MarketPrice
        include Virtus.model

        attribute :id,           Integer
        attribute :currency,     String
        attribute :value_cents,  Integer
      end
    end
  end
end