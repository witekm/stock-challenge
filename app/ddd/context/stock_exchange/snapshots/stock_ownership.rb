module Context
  module StockExchange
    module Snapshots
      class StockOwnership
        def self.save(aggregate)
          db_stock = Stock.find(aggregate.repo.id)

          case delete_action?(aggregate)
            when true then db_stock.update(deleted_at: Time.now)
            when false then db_stock.update(name: aggregate.stock_name,
                                            bearer_id: aggregate.stock_bearer.id,
                                            market_price_id: aggregate.stock_market_price.id)
          end
        end

        private
        def self.delete_action?(aggregate)
          !aggregate.stock_bearer && !aggregate.stock_market_price
        end
      end
    end
  end
end