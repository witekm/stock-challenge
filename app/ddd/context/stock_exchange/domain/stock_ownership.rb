require 'aggregate_root'

module Context
  module StockExchange
    module Domain
      StockCreated     = Class.new(RailsEventStore::Event)
      StockUpdated     = Class.new(RailsEventStore::Event)
      StockSoftDeleted = Class.new(RailsEventStore::Event)

      class StockOwnership
        include AggregateRoot

        attr_reader :repo, :stock_name, :stock_bearer, :stock_market_price

        def initialize(stock_id)
          @repo               = Repos::StockOwnership.new.find(stock_id)
          @stock_bearer       = repo.stock_bearer
          @stock_market_price = repo.stock_market_price
          @stock_name         = repo.stock_name
          @version            = WalletEventStore.count_events_in_stream(stream_name) - 1 #overwrites aggregate_root method
        end

        def create(current_user_id, stock_name, bearer_id, market_price_id)
          apply StockCreated.new({ data: {
              user_id:               current_user_id,
              stock_bearer_id:       bearer_id,
              stock_market_price_id: market_price_id,
              stock_name:            stock_name
          }})
        end

        def update(current_user_id, stock_name, bearer_id, market_price_id)
          apply StockUpdated.new({ data: {
              user_id:               current_user_id,
              stock_bearer_id:       bearer_id,
              stock_market_price_id: market_price_id,
              stock_name:            stock_name
          }})
        end

        def destroy(current_user_id)
          apply StockSoftDeleted.new({ data: {
              user_id: current_user_id
          }})
        end

        on StockCreated, StockUpdated do |event|
          @stock_bearer       = Repos::Bearer.new.find(event.data.fetch(:stock_bearer_id).to_i)
          @stock_market_price = Repos::MarketPrice.new.find(event.data.fetch(:stock_market_price_id).to_i)
          @stock_name         = event.data.fetch(:stock_name)
        end

        on StockSoftDeleted do |event|
          @stock_bearer = nil
          @stock_market_price = nil
          @stock_name = nil
        end

        def stream_name
          "#{self.class.name.demodulize}$#{repo.id}"
        end

        private
        alias :loaded_from_stream_name :stream_name

        def default_event_store
          Rails.configuration.event_store
        end
      end
    end
  end
end
