module Context
  module StockExchange
    module CommandHandlers
      class UpdateStock
        include CommandHandler

        def call(command)
          bearer_id       = Bearer.find_or_create_by(name: command.bearer_name).id
          market_price_id = MarketPrice.find_or_create_by(currency: command.market_price_currency,
                                                          value_cents: command.market_price_value_cents).id

          with_aggregate(aggregate_class, command.stock_id, true) do |stock_ownership|
            stock_ownership.update(command.current_user_id,
                                   command.stock_name,
                                   bearer_id,
                                   market_price_id)
          end
        end

        def aggregate_class
          ::Context::StockExchange::Domain::StockOwnership
        end
      end
    end
  end
end