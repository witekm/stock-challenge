module Context
  module StockExchange
    module CommandHandlers
      class SoftDeleteStock
        include CommandHandler

        def call(command)

          with_aggregate(aggregate_class, command.stock_id, true) do |stock_ownership|
            stock_ownership.destroy(command.current_user_id)
          end
        end

        def aggregate_class
          ::Context::StockExchange::Domain::StockOwnership
        end
      end
    end
  end
end