module Context
  module StockExchange
    module CommandHandlers
      class CreateStock
        include CommandHandler

        def call(command)
          new_stock = ::StockExchange::CreateStockService.call(name: command.stock_name,
                                                              bearer_id: command.bearer_id,
                                                              market_price_id: command.market_price_id)

          return unless new_stock.valid?
          with_aggregate(aggregate_class, new_stock.id, true) do |stock_ownership|
            stock_ownership.create(command.current_user_id,
                                   command.stock_name,
                                   command.bearer_id,
                                   command.market_price_id)
          end
        end

        def aggregate_class
          ::Context::StockExchange::Domain::StockOwnership
        end
      end
    end
  end
end