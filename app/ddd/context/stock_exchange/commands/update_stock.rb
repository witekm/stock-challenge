module Context
  module StockExchange
    module Commands
      class UpdateStock < Command
        attr_accessor :bearer_name,
                      :current_user_id,
                      :market_price_currency,
                      :market_price_value_cents,
                      :stock_name,
                      :stock_id

        validates_presence_of :bearer_name,
                              :current_user_id,
                              :market_price_currency,
                              :market_price_value_cents,
                              :stock_name,
                              :stock_id
      end
    end
  end
end