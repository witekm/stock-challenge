module Context
  module StockExchange
    module Commands
      class SoftDeleteStock < Command
        attr_accessor :current_user_id, :stock_id

        validates_presence_of :current_user_id, :stock_id
      end
    end
  end
end