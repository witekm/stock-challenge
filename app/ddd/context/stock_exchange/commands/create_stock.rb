module Context
  module StockExchange
    module Commands
      class CreateStock < Command
        attr_accessor :bearer_id,:current_user_id, :market_price_id, :stock_name

        validates_presence_of :bearer_id, :current_user_id, :market_price_id, :stock_name
      end
    end
  end
end