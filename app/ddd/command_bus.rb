
module CommandBus
  def execute(command)
    command.validate!
    handler = "::Context::#{command.class.name.split('::').second}::CommandHandlers::#{command.class.name.demodulize}"
    handler.constantize.new.call(command)
  rescue ActiveRecord::RecordNotFound => e
    raise RecordNotFoundError.new(e.message)
  rescue RecordInvalidError => e
    raise
  rescue StandardError => e
    raise RecordGeneralError.new(e.message)
  end
end
