
module CommandHandler
  def with_aggregate(aggregate_class, aggregate_id, use_snapshot = false)
    stream    = "#{aggregate_class.name.demodulize}$#{aggregate_id}"
    aggregate = aggregate_class.new(aggregate_id)
    aggregate = aggregate.load(stream) unless use_snapshot
    yield aggregate

    "Context::#{aggregate_class.name.split('::').second}::Snapshots::#{aggregate_class.name.demodulize}".constantize.save(aggregate) if use_snapshot #changing read model here not to cause issues with data consistency if we would handle that in denormalizers
    aggregate.store
  end
end