
module Repository
  def initialize(data)
    @orm_adapter = data[:orm_adapter]
  end

  private
  attr_reader :orm_adapter

  def map_record
    raise NotImplementedError
  end
end
