
class WalletEventStore
  class << self
    def count_events_in_stream(stream_name)
      ::RailsEventStoreActiveRecord::EventInStream.where(stream: stream_name).count
    end
  end
end