
class Command
  CommandValidationError = Class.new(StandardError)

  include ActiveModel::Model
  include ActiveModel::Validations
  include ActiveModel::Conversion

  def initialize(attributes={})
    super
  end

  def validate!
    raise CommandValidationError, "#{self.class.to_s} - #{errors.messages}" unless valid?
  end

  def persisted?
    false
  end
end