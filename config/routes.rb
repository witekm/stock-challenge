Rails.application.routes.draw do
  namespace :api do
    resources :stocks, only: [:create, :update, :destroy, :index]
  end
end
